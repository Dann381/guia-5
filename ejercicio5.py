import os


# Funcion de traspaso de datos del archivo a arreglos
def obtener_paises():
    archivo = open("co2_emission.csv")
    totalpaises = []
    paises = []
    stringco2 = []
    pais = ""
    for linea in archivo:
        pais = linea.split(",")[0]
        if pais == 'Entity' or pais == 'World' or pais == 'Statistical differences':
            pass
        else:
            tons = linea.split(",")[3]
            totalpaises.append(pais)
            stringco2.append(tons)
            if pais not in paises:
                paises.append(pais)
    archivo.close()
    return paises, totalpaises, stringco2


# Funcion para saber la cantidad de registros
def cantidad_registros(paises, totalpaises):
    registros = []
    for pais in paises:
        cantidad = 0
        for elemento in totalpaises:
            if pais == elemento:
                cantidad = cantidad + 1
        registros.append(cantidad)
    return registros


# Funcion para convertir las toneladas de CO2 a numeros decimales
def convertir_datos(stringco2):
    totalco2 = []
    for tons in stringco2:
        totalco2.append(float(tons))
    return totalco2


# Funcion para obtener la suma total de CO2 emitido por pais
def sumar_tons(paises, totalpaises, totalco2):
    tonsporpais = []
    i = 0
    for pais in paises:
        temp = 0
        for elemento in totalpaises:
            if elemento == pais:
                temp = temp + totalco2[i]
                i = i + 1
        tonsporpais.append(temp)
    return tonsporpais


# Funcion para obtener el promedio de emisiones de CO2 de cada pais
def promedio_pais(paises, registros, tonsporpais):
    promediopais = []
    i = 0
    for elemento in paises:
        temp = tonsporpais[i]/registros[i]
        promediopais.append(temp)
        i = i + 1
    return promediopais


# Funcion para obtener el menor promedio de emisiones de CO2
def promedio_max(promediopais):
    minimo = min(promediopais)
    posicion = promediopais.index(minimo)
    return minimo, posicion


# Funcion principal
os.system("clear")
paises, totalpaises, stringco2 = obtener_paises()
registros = cantidad_registros(paises, totalpaises)
totalco2 = convertir_datos(stringco2)
tonsporpais = sumar_tons(paises, totalpaises, totalco2)
promediopais = promedio_pais(paises, registros, tonsporpais)
minimo, posicion = promedio_max(promediopais)
print("El país con el menor promedio de emision de CO2 es", paises[posicion])
print("El promedio de emision de CO2 es de", minimo, "toneladas")
