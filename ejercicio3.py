import os


# Funcion de traspaso de datos del archivo a arreglos
def abrir_archivo():
    archivo = open("co2_emission.csv")
    CO2 = []
    contador = 0
    tons = ""
    for linea in archivo:
        contador = contador + 1
        limpio = linea.strip()
        tons = limpio.split(",")[3]
        CO2.append(tons)
    archivo.close()
    contador = contador - 1
    CO2.pop(0)
    return CO2, contador


# Funcion para convertir todos los elementos de la lista CO2 a numeros decimales
def convertir_datos(CO2):
    toneladas = []
    for elementos in CO2:
        toneladas.append(float(elementos))
    return toneladas


# Funcion para calcular el promedio
def promedio(toneladas, contador):
    suma = 0
    for elementos in toneladas:
        suma = suma + elementos
    promedio = suma/contador
    print("El promedio de toneladas emitidas totales por todos los países es de",promedio,"toneladas")
        



# Funcion princial
os.system("clear")
CO2, contador = abrir_archivo()
toneladas = convertir_datos(CO2)
promedio(toneladas, contador)
