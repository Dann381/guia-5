import os


# Funcion de traspaso de datos del archivo a arreglos
def abrir_archivo():
    archivo = open("co2_emission.csv")
    totalpaises = []
    paises = []
    pais = ""
    for linea in archivo:
        pais = linea.split(",")[0]
        totalpaises.append(pais)
        if pais not in paises:
            paises.append(pais)
    archivo.close()
    paises.remove("Entity")
    totalpaises.remove("Entity")
    return paises, totalpaises


# Funcion para saber que pais tiene mayor cantidad de registros
def cantidad_registros(paises, totalpaises):
    cantidad = []
    for pais in paises:
        registros = 0
        for elemento in totalpaises:
            if pais == elemento:
                registros = registros + 1
        cantidad.append(registros)
    maximo = max(cantidad)
    posicion = cantidad.index(maximo)
    return cantidad, posicion


# Funcion princial
os.system("clear")
paises, totalpaises = abrir_archivo()
cantidad, posicion = cantidad_registros(paises, totalpaises)
print("País con mayor cantidad de registros:", paises[posicion])
print("Cantidad de registros:", cantidad[posicion])
