import os


# Funcion de traspaso de datos del archivo a arreglos
def obtener_paises():
    archivo = open("co2_emission.csv")
    totalpaises = []
    paises = []
    stringco2 = []
    pais = ""
    datos = 0
    for linea in archivo:
        pais = linea.split(",")[0]
        if pais == 'Entity' or pais == 'World' or pais == 'Statistical differences':
            pass
        else:
            tons = linea.split(",")[3]
            totalpaises.append(pais)
            stringco2.append(tons)
            datos = datos + 1
            if pais not in paises:
                paises.append(pais)
    archivo.close()
    return paises, totalpaises, stringco2, datos


# Funcion para saber la cantidad de registros
def cantidad_registros(paises, totalpaises):
    registros = []
    for pais in paises:
        cantidad = 0
        for elemento in totalpaises:
            if pais == elemento:
                cantidad = cantidad + 1
        registros.append(cantidad)
    return registros


# Funcion para convertir las toneladas de CO2 a numeros decimales
def convertir_datos(stringco2):
    totalco2 = []
    for tons in stringco2:
        totalco2.append(float(tons))
    return totalco2


# Funcion para obtener la suma total de CO2 emitido por pais
def sumar_tons(paises, totalpaises, totalco2):
    tonsporpais = []
    i = 0
    for pais in paises:
        temp = 0
        for elemento in totalpaises:
            if elemento == pais:
                temp = temp + totalco2[i]
                i = i + 1
        tonsporpais.append(temp)
    return tonsporpais


# Funcion para obtener el promedio de emisiones de CO2 de cada pais
def promedio_pais(paises, registros, tonsporpais):
    promediopais = []
    i = 0
    for elemento in paises:
        temp = tonsporpais[i]/registros[i]
        promediopais.append(temp)
        i = i + 1
    return promediopais


# Funcion para saber el promedio de todas las emisiones de CO2, y para obtener el rango de +-10%
def promedio_total(totalco2, datos):
    sumatotal = 0
    for tons in totalco2:
        sumatotal = sumatotal + tons
    promediototal = sumatotal/datos
    porciento = promediototal * 0.1
    maxrange = promediototal + porciento
    minrange = promediototal - porciento
    return promediototal, sumatotal, maxrange, minrange
 

# Funcion para determinar que paises están dentro del rango
def paises_rango(maxrange, minrange, paises, promediopais):
    posicion = 0
    paisesrango = []
    promediosrango = []
    for promedio in promediopais:
        if promedio >= minrange and promedio <= maxrange:
            paisesrango.append(paises[posicion])
            promediosrango.append(promediopais[posicion])
        posicion = posicion + 1
    return promediosrango, paisesrango




# Funcion principal
os.system("clear")
paises, totalpaises, stringco2, datos = obtener_paises()
registros = cantidad_registros(paises, totalpaises)
totalco2 = convertir_datos(stringco2)
tonsporpais = sumar_tons(paises, totalpaises, totalco2)
promediopais = promedio_pais(paises, registros, tonsporpais)
promediototal, sumatotal, maxrange, minrange = promedio_total(totalco2, datos)
promediosrango, paisesrango = paises_rango(maxrange, minrange, paises, promediopais)
i = 0
print("Los países que están dentro del rango del promedio, con un margen de"
      " +-10 % son:")
for elementos in paisesrango:
    print(elementos,"=",promediosrango[i],"toneladas")
    i = i + 1

